# Privacy online

## Title slide
- Loads of people claim to be interested, few actually know anything
- How far the right to privacy extends is frequently challenged
- few people have any clue what they are talking about and I don't consider myself one of them

## Overview

Brief overview of my talk
I'm not gonna repeat the usual stuff, so this is going to be more esoteric

## Why do we care

- People are weird and care about such stuff
- Knowledge is power
    - Tracking
    - Blackmail
    - Stalking
- Can be leaked and abused even if you trust a company:
- Stays forever, new encryption allows access, keep copies
- **Snapchat is not secure** Took me 30m to bypass on pc 

## Threats
- Incompetent companies
- Bans on Encryption(EU, US, E2E, phones, locale)(csam apple)
- Fingerprinting(GPU and co)
- Face recognition(de-anonymize)
- Weak passwords -> Bitwarden, 2FA, don't reuse, high entropy(horse battery staple)
- If it ain't encrypted it's already public(***Elena***)

```
if(isTrackingAllowed){
    track();
} else{
    track();
}
```

## Mitigation

or: the far more interesting part
NOT: a comprehensive guide
Take with grain of salt, composed by me

### Fingerprinting
- Use plugins e.g. LibreJS, user agent spoofer
- block autoplay
- block cross origin tracking
- don't: use do not track
- Cookie Separation
- Tor/I2P
- Tails for those less technically inclined
- Sandboxing: apps, os, browser
- restrict info+spoof=useless fingerprint
- Browser settings

### Encryption
- Bitlocker is better than nothing
- One of the few things MacOS does right
- Use LUKS on linux
- GPG(OpenPGP): mail, signature, other encryption sym/asym...
- Do not trust *anyone*
- **Physical tokens!**
- When in doubt use asymmetric 
- Bigger sometimes is better
- Don't for the love of god use closed source encryption software

### 2FA
- Physical tokens(gpg, passwords, certificates secure storage)
- Biometric keys are very convenient
- Use everywhere
- Say no to sms
- Sms in general sucks, use rcs or sthg
- demonstrate yubikey


### Secure Software and OS
- Linux
- No, mac doesn't count
- Sandbox *everything*
- Minimum Permissions(Windows is the only one where that doesn't work)
- Block network access
- OSS is superior
- Apple backdoors(csam, find my with phone turned off)
- Windows does not have a backdoor, it is a wide open front door and a virus all on its own

## VPNS

### Problems
- Misleading ads(are not really private on their own)
- overused(if you use it for everything that's not really helpful)
- Some feature(dedicated ip) make the entire vpn useless
- Cheap options *There is no such thing as a decent free VPN!*
- DNS queries often unencrypted/bypass vpn due to browser defeating the point
- fingerprinting still possible, sometimes easier 'cause fewer people use VPN's
- if you are going to log in anyway, why bother

### What are they
- Forward requests from a different server
- Not intended for this
- Not designed to be used like this


### Alternatives
- **Tor**

### Resources
- arch/gentoo wiki for general stuff
- mental outlaw - use with caution loads of random stuff
- pwned if you are reusing passwords
- tor (see above)
- browserleaks( a bit behind at times but better than nothing)

### Conclusion
- It's possible
- Ten minutes not enough
- no one listens
- use vim
- If you want to be 100% secure use templeOS Terry was right]

## RMS
Do not ask questions or look him up, his personality ranges from weird to how is he not in prison
